---
layout: page
title: Experiência
permalink: /experience
lang: pt_br
---

## **Silver Logic** *(Remoto)*
**Full-Stack Developer** *(Jun 2018 – Presente)*
- Desenvolvimento de aplicações web utilizando **React.js/NextJs** para o frontend e **Python/Django** para o backend.
- Experiência em **DevOps**, com deploy automatizado utilizando **Ansible** e **Docker**.
- Reconhecido como **Funcionário do Mês** em **junho de 2023**.
- Atuação como **Líder Técnico** do Time Mars na empresa.
- Selecionado para liderança do grupo de **DevOps**.
- Automatização de fluxos de trabalho criando ferramentas de linha de comando para uso interno da empresa.

## Hack your future
**Voluntário** *(Mai 2024 – Presente)*
- Ensino de programação para refugiados.
- Revisão de códigos e feedbacks.
- Q&A para módulos novos de aprendizado.

## **Scopus** *(São Paulo, BR)*
**Developer** *(Fev 2016 – Jan 2018)*
- Desenvolvimento de **Backend-for-Frontend** para aplicação Android utilizando **Express/GraphQL**.
- Trabalhou com **Micro-serviços** em **Node.js/Express** e **Python/Flask/Django**.
- Gerenciamento de banco de dados **NoSQL** utilizando **Redis** e **MongoDB**.
- Controle de **CI/CD** com **GitLab Pipelines**.
- Organização de imagens Docker para cada micro-serviço com **Docker Compose** e **Swarm**.
- Deploy em servidores **Ubuntu** e **CentOS** na **AWS**.

## **Scopus** *(São Paulo, BR)*
**Programa de Trainee** *(Mar 2015 – Dez 2015)*
- Localização e automação de drones em ambientes internos sem GPS.
- Tecnologias utilizadas: **C++ e ARToolKit** para localização.
- Comunicação via **GStreamer e MongoDB**.
- Interface em tempo real com **Node.js/Meteor**.
- Simulação de voo com **Python/Blender**.
- **Graduado com Honra**, classificado no **Top 5** da turma.

## **Dagood - Website** *(São Paulo, BR)*
**Co-Fundador** *(Mai 2012 – Out 2013)*
- Criou o **Dagood**, um motor de busca para restaurantes, bares e clubes.
- Desenvolveu o backend utilizando **PHP/Zend Framework 2** e o frontend com **HTML/CSS/JavaScript**.

## **Engenharia Eletrônica e de Computação**
**Escola Politécnica da Universidade de São Paulo (USP), Brasil**
- **Dissertação:** Localização e automação de drones em ambiente interno.
- Matérias principais: Estruturas de Dados, Linguagens e Compiladores, Engenharia de Software, Redes de Computadores, Modelagem e Simulação de Sistemas Computacionais, Sistemas Operacionais e Segurança da Informação.
