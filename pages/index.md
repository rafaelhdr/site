---
layout: page
permalink: /
lang: en
---

## Hello, I am Rafael

I am a software engineer, and I like to write about programming, productivity, and life. I am from Brazil, and I am currently living in the Netherlands.

I am a big fan of Python, Vim, and Linux. I am always trying to improve my skills and share my knowledge with others.
