---
layout: page
permalink: /
lang: pt_br
---

## Olá, eu sou Rafael

Sou engenheiro de software e gosto de escrever sobre programação, produtividade e vida. Sou do Brasil e atualmente moro na Holanda.

Sou um grande fã de Python, Vim e Linux. Estou sempre tentando melhorar minhas habilidades e compartilhar meu conhecimento com os outros.
