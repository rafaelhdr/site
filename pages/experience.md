---
layout: page
title: Experience
permalink: /experience
lang: en
---

## **Silver Logic** *(Remote)*
**Full-Stack Developer** *(Jun 2018 – Present)*
- Development of web applications using **React.js/Next.js** for the frontend and **Python/Django** for the backend.
- Experience in **DevOps**, with automated deployment using **Ansible** and **Docker**.
- Recognized as **Employee of the Month** in **June 2023**.
- Served as **Technical Leader** of the Mars Team within the company.
- Selected for leadership of the **DevOps** group.
- Automated workflows by creating command-line tools for company-wide use.

## **Hack Your Future** *(Remote)*
**Volunteer** *(May 2024 – Present)*
- Teaching programming to refugees.
- Code review and feedback.
- Q&A sessions for new learning modules.

## **Scopus** *(São Paulo, BR)*
**Developer** *(Feb 2016 – Jan 2018)*
- Developed **Backend-for-Frontend** connection for Android application using **Express/GraphQL**.
- Worked on **Micro-services** with **Node.js/Express** and **Python/Flask/Django**.
- Managed **NoSQL** Databases using **Redis** and **MongoDB**.
- Controlled **CI/CD** using **GitLab Pipelines**.
- Organized Docker images for each Micro-service using **Docker Compose** and **Swarm**.
- Deployment to **Ubuntu** and **CentOS** servers in **AWS**.

## **Scopus** *(São Paulo, BR)*
**Trainee Program** *(Mar 2015 – Dec 2015)*
- Drone localization and automation in indoor space without GPS.
- Technologies used: **C++ and ARToolKit** for localization.
- Communication via **GStreamer and MongoDB**.
- Real-time user interface with **Node.js/Meteor**.
- Flight simulator with **Python/Blender**.
- **Graduated with Honors**, ranked in the **Top 5** of the class.

## **Dagood - Website** *(São Paulo, BR)*
**Co-Founder** *(May 2012 – Oct 2013)*
- Created **Dagood**, a search engine for restaurants, bars, and clubs.
- Developed the backend using **PHP/Zend Framework 2** and the frontend with **HTML/CSS/JavaScript**.

## **Electronic and Computer Engineering**
**Polytechnic School of the University of São Paulo (USP), Brazil**
- **Dissertation:** Indoor location and automation of drones.
- Key subjects: Data Structures, Languages and Compilers, Software Engineering, Computer Networks, Modeling and Simulation of Computer Systems, Operating Systems, and Information Security.
