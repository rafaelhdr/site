---
layout: post
title:  "rafaelhdr agora em Jekyll"
date:   2017-11-20 20:00:00 -0300
categories: rafaelhdr jekyll
permalink: rafaelhdr-new-website-with-jekyll
lang: pt_br
---
# Novo rafaelhdr

E de novo estou mudando meu website. Primeiro, usei WordPress, migrei para o Django e agora estou usando o Jekyll.

## Motivação

Eu queria ele bilíngue e simples. Além do mais, sempre gosto de aprender coisa nova.\

## Por que Jekyll

Ele cobre toda a minha motivação.

Sempre quis aprendê-lo pois um monte de gente o usa para páginas no GitHub, razão a mais para usá-lo (em relação a outro gerador de sites estáticos).

## Vantagens

* Meu site agora é open-source;
* Barato (S3 ao invés de instância EC2);
* Versionamento (git);
* CI/CD (GitLab Pipeline)
* Alta disponibilidade (99,99% [de acordo com AWS](https://aws.amazon.com/s3/faqs/));
* Rápido (testado com [PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/) - 63/100 melhorou para 89/100);
* É Markdown.

## E mais

O site é o mesmo. Estou mantendo o conteúdo antigo, mas sem traduzí-lo. Novo conteúdo será bilíngue.

O layout não é o melhor. É simples (o padrão do jekyll). Espero trabalhar nisso algum dia, mas não agora.

Espero postar mais. Estou procurando trabalho novo e acredito que será uma boa oportunidade para me apresentar. Meu próximo post será a integração de um website estático com o Cloudflare (que estou usando por agora).

Espero que todos gostem :)
