---
layout: post
title:  "Primeiras impressões do Linux Mint"
date:   2015-01-05 15:30:00 -0300
categories: linux mint
permalink: blog/linux/primeiras-impressoes-do-linux-mint
lang: pt_br
---
# Primeiras impressões do Linux Mint

Bom, já uso normalmente o Ubuntu, e nas últimas semanas usei o Elementary OS e o Debian. Agora foi a vez do **Linux Mint**.

![linux mint screenshot]({{"/assets/posts/2015-01-05-primeiras-impressoes-do-linux-mint/linux-mint-screenshot.png" | absolute_url }})

E esse é o screenshot da tela inicial do Mint (acho que a única mudança do original são os atalhos em baixo). 

Sobre a **instalação**, achei boa. Tive uma má impressão do Debian, mas no Mint foi super tranquilo. Fiz o teste sem instalá-lo, e então mandei colocar no disco rígido, e foi sem erros.

Acho que é **uma boa distribuição para começar**. Não tive muitos problemas (apenas o básico de se acostumar), mas ele veio todo pronto, e não é difícil instalar as coisas. Me lembro apenas do problema com PPAs. Fui seguir os passos normais para instalar o Sublime Text 3, e não foi. Os passos eram pra Ubuntu, mas achei que devia ser fácil, já que essas são baseadas no Debian. Mas não tem problema. Acabei instalando na mão mesmo, e foi.

Sobre a **performance**, eu não gostei muito. Achei que fez diferença, que estava um pouco lerdo. Mas consegui melhorar legal com um kernel mais novo. Por sinal, vou divulgar um script muito bom que achei pra atualizar o kernel: [Script para atualizar o kernel do vivaolinux](http://www.vivaolinux.com.br/dica/Atualizacao-rapida-do-kernel-317-no-Debian-Ubuntu-e-Linux-Mint).

Usei a versão com o **Cinnamon**. Ela é legal, e bem organizadinha. Conseguimos fazer busca por aplicativos bem fácil:

![linux mint screenshot]({{"/assets/posts/2015-01-05-primeiras-impressoes-do-linux-mint/linux-mint-cinnamon-screenshot.png" | absolute_url }})

O detalhe é que dá alguns paus, como não poder gravar na USB ([link](http://forums.linuxmint.com/viewtopic.php?f=208&t=176546)). Precisei instalar o Nautilus apenas para isso.

Algo que fiz questão de mudar é a mudança de **workspaces**. Nele era muito devagar, e apenas horizontal (tem uns apps para deixar fazer vertical, mas não funcionou por aqui), e mudava apenas de um em um (por exemplo, se estou no workspace 1, e quero ir pro 4, preciso passar pelo 2 e 3). Então resolvi copiar os atalhos que já tinha visto no Elementary OS, que achei muito bons (no caso, se eu aperto **Super + n**, ele muda pro workspace n).

E há algo realmente **irritante**, que são os sons. Se você aumenta/diminui o volume, tem um efeito. Muda de workspace, tem outro. Em diversas ações, tem um barulho chato. Sorte que é fácil de consertar. Vai no Menu, digite sound (só tem uma opção), e então é só ir na aba de sound effects e desativar o que quiser.

Sobre a **comunidade**, eu não precisei recorrer a nada. Claro que há bastante conteúdo por aí, e li em alguns lugares falando que a comunidade está maior até que a do Ubuntu (embora eu não acredite nisso, mas provavelmente é uma comunidade grande mesmo assim), mas é uma distribuição bem simples de usar, e que não dá dor de cabeça.

Bom, é isso. Gostei dele, mas achei lento (de forma resolvível). Sugiro para você quem quer começar com o mundo Linux. Não acredito que você irá se frustrar.
