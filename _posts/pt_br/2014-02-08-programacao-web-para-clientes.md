---
layout: post
title:  "Programação web para clientes"
date:   2014-02-08 12:00:00 -0300
categories: web clientes
permalink: blog/about/programacao-web-para-clientes
lang: pt_br
---
# Veja o básico antes de contratar um freelancer

Tem diversos assuntos que é interessante saber quando vai conversar com um desenvolvedor. Com certeza, eles vão explicar, mas é tanta informação, que seria mais interessante ir sabendo um pouco, e ter menos para assimilar. Além disso, o assunto é tão comum para os programadores que é bastante comum passar batido em algo.

## O que você precisa saber antes da conversa

Se você não sabe nada, precisa saber o que é **domínio** e **servidor**.

* Domínio - Esse é o nome de seu site. Ele é composto apenas pelo nome e sua extensão final. Por exemplo, meu domínio é rafaelhdr.com.br. O www não faz parte de meu domínio. Você precisa saber se o nome que deseja registrar está disponível, ou se alguém já o usa. Para os brasileiros, verifique com o registro.br. Logo na primeira página há um espaço para verificar. E se estiver livre, sugiro que você mesmo registre, pois ficará sobre seu próprio controle esse nome, e não na mão de outras pessoas. *
* Servidor - É o computador que guarda as informações de seu site. Sobre o servidor, geralmente o próprio programador quem irá se preocupar com ele. Se você quiser um site simples, e ele cobrar caro, conheço um que vai cobrar cerca de R$10 ao mês (cobrando semestralmente). Acesse gdihost. Eu possuo servidor próprios para hospedar sites, mas normalmente faço isso com aqueles que eu mesmo realizo o serviço.

\* Se quiser saber como registrar domínio, veja o [post que escrevi sobre isso]({% post_url pt_br/2014-02-12-como-registrar-dominio-com-br %}).

## Tópicos interessantes

Quando você for fazer um site bem simples, até mesmo seu sobrinho pode fazer.

Como site simples, eu quero dizer aqueles compostos de algumas páginas, que você não atualiza nunca, ou atualiza pouco.

Mas se estiver interessado em um site mais complexo, que precisa de continuidade e não possa parar do nada, recomendo pessoas com mais experiências. Para isso, sugiro que você saiba um pouco mais sobre as tecnologias:

### Linguagem

A linguagem é uma tecnologia base que o programador pode utilizar para desenvolver seu site.

A escolha é feita mais por gosto da equipe. Pode-se falar que usa PHP, ou Python, ou Ruby, ou Asp, entre outras. Eu, particularmente, tenho umas que prefiro em relação a outras, mas nesse critério, acredito que não faça diferença se decidir usar uma ou outra, desde que entregue o trabalho. Porém, é interessante que a pessoa use no mínimo algum framework para isso.

### Framework

Framework é uma estrutura padrão para desenvolver o site. Ele dá é uniformidade e organização no desenvolvimento web. Com ele, em geral, você pode contratar um outro desenvolvedor para continuar o trabalho de seu atual sem tantos problemas. Além disso, trará mais segurança no código.

A pessoa que não utiliza provavelmente lhe dirá algo como "utilizo php puro, pois framework tira o desempenho". Essa questão de tirar o desempenho é verdade, mas hoje em dia os computadores estão tão poderosos, que as pessoas não sentem essa diferença. Por exemplo, acabei de testar essa minha página, e meu site levou 14 ms para processar. Levamos mais tempo para trazer a informação do que para o framework rodar.

Em resumo, vou dizer que framework é ponto positivo para seu programador.

### CMS

Há também gerenciadores de conteúdo - CMS (do inglês, Content Management System). Eles são sites que possuem uma área administrativa de uso simples para que os clientes possam gerenciar o conteúdo de seu site.

Como exemplo, vou citar o wordpress. É o gerenciador mais famoso, que nasceu para servir como blog, mas cresceu tanto que o utilizam para muito mais coisas (como sites comuns, e também e-commerces).

Eu, particularmente, não gosto muito de utilizá-lo, pois em muitos casos acho que ele é matar formiga com canhão, mas não nego que é uma ótima ferramenta que supre grande parte de nossas necessidades.

Não sou da área de segurança, mas mesmo assim vejo diversos bugs referentes à wordpress, e como ele ficou famoso, vejo pessoas se aproveitando disso para atacar o site. Mas acredito que se for bem gerenciado, não deve trazer problemas.

## Resumo

Sugiro que saiba o que é domínio e servidor. Além disso, se tiver um site mais complexo, se preocupe um pouco mais com a tecnologia, contratando profissionais que utilizem um framework ou CMS.

Acredito que pude dar uma primeira passada nas informações. Tenho bastante mais a falar, mas já foi muita coisa para uma primeira conversa.
Em breve coloco mais assuntos para quem se interessar mais.
