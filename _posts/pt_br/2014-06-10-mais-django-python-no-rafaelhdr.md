---
layout: post
title:  "Mais Django Python no rafaelhdr"
date:   2014-06-10 22:00:00 -0300
categories: rafaelhdr django python
permalink: blog/about/mais-django-python-no-rafaelhdr
lang: pt_br
---
# Um pouco sobre Django Python

Ele nasceu em 2005, quando estavam desenvolvendo para o Lawrence Journal-World. Depois puderam aproveitar para lançar o framework. Hoje em dia é mantido pela Django Software Foundation.

Seu objetivo é de poder desenvolver componentes plugáveis, de forma ágil, e sem repetição (Don't Repeat Yourself - DRY).

Alguns exemplos de sites que utilizam o Django, é o [PInterest](https://br.pinterest.com/), [Instagram](http://instagram.com/), [Mozilla](http://www.mozilla.org/), [The Washington Times](http://www.washingtontimes.com/) e [Disqus](http://disqus.com/).

## Como comecei

Conheci o Python Django em 2008, na [Poli Júnior](http://polijr.com.br/), em que eu trabalhava no Núcleo de Tecnologia, e desenvolvia Sistemas de Informação. Na época, eu trabalhava apenas com PHP (puro), e mal sabia da existência de frameworks.

Foi o começo de uso de Django na empresa júnior. Acredito que escolheram ele por ser de fácil aprendizado, uma ótima comunidade e baixa curva de aprendizado (e provavelmente haviam outros motivos). Gostei muito dele. Facilitava bastante muitas das coisas que programávamos.

## Vantagens

Há algum tempo, fui procurar algo semelhante para o PHP. Estudei alguns frameworks. Conheci mais a funco o Zend Framework (1 e 2) e Code Igniter. Vi um pouco do Phalcon e do Laravel. Mas confesso que acho o Django muito acima em diversos aspectos.

* Ele é DRY. Eu não preciso ficar repetindo código
* Tem uma ótima comunidade, e que ainda é bem ativa
* O ORM é muito bom (nesse caso, estou comparando apenas com o Doctrine 2, mas achei o do Django muito melhor)
* Diversos comandos prontos (python manage.py comando)
* Preocupação da comunidade em ensinar mais do que a linguagem (há muitas dicas no próprio primeiro tutorial para ter boas práticas)

Esses foram os fatores principais, embora eu possa achar outros muito bons também.

Bom, espero ter ajudado. Se quiser ver algo em específico, deixe nos comentários.
